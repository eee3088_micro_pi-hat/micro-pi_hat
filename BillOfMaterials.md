# Bill of Materials
## The following table shows the quantity and type components used in the project. This are the "ingredients" to the design of the Piano piHat. Similar components without a big difference to the specified components can be used to repace the given component only if the difference in within the speciefied values of the given componet.

| component | reference  | value  | Description  |
|-----------------|:-------------|:---------------:|---------------:|
| op-amp | Dual supply IC 741 | 1      | astable mode  |
| capacitors     | C1=C2=C3          | 10nF,10micro,      | unpolazed caps           |
| Resistors      | R1=1k,R2=R3=10k         | R1=1K,R2=10=R3,R5=R6=R7=10K             | Unpolarised resistors            |
||
|  LED    |      D1,D2,D3     |   1N4002 LED              |     Forward voltage=5V        |
| LED       |        D4      |          LxxR5000H LED       |       Forward voltage of 3V         |Luminous Intensity=50 mcd ,Forward voltage=3V
|Transformer| L1,L2 | 400m,1m |unpolarised inductors
| 555TIMER     |   555TIMER           |     1            | connected in Astable mode          |
{: .custom-class #custom-id}
