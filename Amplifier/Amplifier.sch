EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title "Amplifier"
Date "6/ 3/2021"
Rev ""
Comp "University of Cape Town"
Comment1 "Makhado Andani"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R_Small_US R7
U 1 1 60B76E4B
P 5000 4250
F 0 "R7" H 5068 4296 50  0000 L CNN
F 1 "R_Small_US" H 5068 4205 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 5000 4250 50  0001 C CNN
F 3 "~" H 5000 4250 50  0001 C CNN
	1    5000 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R6
U 1 1 60B770BD
P 5000 3750
F 0 "R6" H 5068 3796 50  0000 L CNN
F 1 "R_Small_US" H 5068 3705 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 5000 3750 50  0001 C CNN
F 3 "~" H 5000 3750 50  0001 C CNN
	1    5000 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R5
U 1 1 60B77AC3
P 6050 3500
F 0 "R5" H 6118 3546 50  0000 L CNN
F 1 "R_Small_US" H 6118 3455 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 6050 3500 50  0001 C CNN
F 3 "~" H 6050 3500 50  0001 C CNN
	1    6050 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R4
U 1 1 60B77E2F
P 6050 4500
F 0 "R4" H 6118 4546 50  0000 L CNN
F 1 "R_Small_US" H 6118 4455 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 6050 4500 50  0001 C CNN
F 3 "~" H 6050 4500 50  0001 C CNN
	1    6050 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 60B78337
P 6900 4550
F 0 "C2" H 7015 4596 50  0000 L CNN
F 1 "C" H 7015 4505 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P1.50mm" H 6938 4400 50  0001 C CNN
F 3 "~" H 6900 4550 50  0001 C CNN
	1    6900 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 3600 6050 3750
Wire Wire Line
	6050 4200 6050 4300
Wire Wire Line
	5750 4000 5000 4000
Wire Wire Line
	5000 4000 5000 3850
Wire Wire Line
	5000 4000 5000 4150
Connection ~ 5000 4000
Wire Wire Line
	5000 4350 5000 4600
Wire Wire Line
	5000 4600 6050 4600
Wire Wire Line
	6050 4600 6050 4700
Wire Wire Line
	6050 4700 6900 4700
Connection ~ 6050 4600
Wire Wire Line
	6900 4400 6900 4300
Wire Wire Line
	6900 4300 6050 4300
Connection ~ 6050 4300
Wire Wire Line
	6050 4300 6050 4400
Wire Wire Line
	6050 3400 6050 3350
Wire Wire Line
	4550 3350 5000 3350
Wire Wire Line
	5000 3350 5000 3650
Connection ~ 5000 3350
Wire Wire Line
	5000 3350 6050 3350
Wire Wire Line
	6050 3750 6900 3750
Connection ~ 6050 3750
Wire Wire Line
	6050 3750 6050 3800
Text HLabel 6900 3750 2    50   Input ~ 0
A_Vout
Text HLabel 4550 3350 0    50   Input ~ 0
R_Vin
$Comp
L Device:Q_NPN_BCE Q1
U 1 1 60B79928
P 5950 4000
F 0 "Q1" H 6141 4046 50  0000 L CNN
F 1 "Q_NPN_BCE" H 6141 3955 50  0000 L CNN
F 2 "Package_TO_SOT_THT:NEC_Molded_7x4x9mm" H 6150 4100 50  0001 C CNN
F 3 "~" H 5950 4000 50  0001 C CNN
	1    5950 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 4600 4550 4600
Connection ~ 5000 4600
Text HLabel 4550 4600 0    50   Input ~ 0
GND
$EndSCHEMATC
