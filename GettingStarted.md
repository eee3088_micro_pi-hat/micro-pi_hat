# Get Started with Piano micro PiHat

## Introduction

In this guide, we will build a simple digital piano on our Piano micro PiHat.

## STEP 1

•	Setting up your Piano micro PiHat with the host Operation system that will manage the communications.

## STEP 2

•	Setting up the digital Piano to the host operation system, which pulls in all necessary dependencies for the application.


Once the set up is complete, your micro PiHat will start with the application.

## What you will need

•	A micro PiHat model                                                    

•	A 4GB or larger microSD card

•	A micro USB cable (for power supply)

