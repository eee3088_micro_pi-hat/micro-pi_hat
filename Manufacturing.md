# How to  manufacture
## Step 1
Go to the specifications and requirements to choose the relevant materials.
The link to this is as follows: https://gitlab.com/eee3088_micro_pi-hat/micro-pi_hat/-/tree/main/Requirements%20and%20Specifications

## Step 2
Choose the specified materials and Go to the Bill Of Materials to check the types of components required and the estimated pricing.
## Step 3
Go to the PCB piano circuit board to generate the footprint files and copy them.
https://gitlab.com/eee3088_micro_pi-hat/micro-pi_hat/-/tree/main/MICRO%20PI%20HAT%20PCB
# Step 4
Go to the libraries' files and copy them along with the BOM and PCB board schematic.
## Step 5 (optional)
Go to PCB view, open the amplifier file and the power supply(switching). Get familiar with the composition of the piHat and how each stage work. The links to the schematics are as follows:
1. Amplifier https://gitlab.com/eee3088_micro_pi-hat/micro-pi_hat/-/tree/main/Amplifier
2. Power supply https://gitlab.com/eee3088_micro_pi-hat/micro-pi_hat/-/tree/main/Power%20Supply
3. LED https://gitlab.com/eee3088_micro_pi-hat/micro-pi_hat/-/tree/main/LEDs%20schematic

## Step 6
Compile a file with the above mentioned files in it.
## Step 7
Send the file in step 6 to the manufacture of choice. The file should contain all the relevant mentioned links.
