EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title "PowerRegulator"
Date "6/ 3/2021"
Rev ""
Comp "University of Cape Town"
Comment1 "Clement Malakalaka"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:D D3
U 1 1 60B6D303
P 7000 3650
F 0 "D3" V 6954 3729 50  0000 L CNN
F 1 "D" V 7045 3729 50  0000 L CNN
F 2 "Diode_THT:D_A-405_P2.54mm_Vertical_AnodeUp" H 7000 3650 50  0001 C CNN
F 3 "~" H 7000 3650 50  0001 C CNN
	1    7000 3650
	0    1    1    0   
$EndComp
$Comp
L Device:D D4
U 1 1 60B6E184
P 7000 4150
F 0 "D4" V 7046 4071 50  0000 R CNN
F 1 "D" V 6955 4071 50  0000 R CNN
F 2 "Diode_THT:D_A-405_P2.54mm_Vertical_AnodeUp" H 7000 4150 50  0001 C CNN
F 3 "~" H 7000 4150 50  0001 C CNN
	1    7000 4150
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D2
U 1 1 60B6E47D
P 7600 3650
F 0 "D2" V 7646 3571 50  0000 R CNN
F 1 "D" V 7555 3571 50  0000 R CNN
F 2 "Diode_THT:D_A-405_P2.54mm_Vertical_AnodeUp" H 7600 3650 50  0001 C CNN
F 3 "~" H 7600 3650 50  0001 C CNN
	1    7600 3650
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D1
U 1 1 60B6E9EC
P 7600 4150
F 0 "D1" V 7554 4229 50  0000 L CNN
F 1 "D" V 7645 4229 50  0000 L CNN
F 2 "Diode_THT:D_A-405_P2.54mm_Vertical_AnodeUp" H 7600 4150 50  0001 C CNN
F 3 "~" H 7600 4150 50  0001 C CNN
	1    7600 4150
	0    1    1    0   
$EndComp
$Comp
L Device:D D5
U 1 1 60B6EFDD
P 9000 4100
F 0 "D5" V 8954 4179 50  0000 L CNN
F 1 "D" V 9045 4179 50  0000 L CNN
F 2 "Diode_THT:D_A-405_P2.54mm_Vertical_AnodeUp" H 9000 4100 50  0001 C CNN
F 3 "~" H 9000 4100 50  0001 C CNN
	1    9000 4100
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C1
U 1 1 60B6FE05
P 8250 4100
F 0 "C1" H 8342 4146 50  0000 L CNN
F 1 "C_Small" H 8342 4055 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P1.50mm" H 8250 4100 50  0001 C CNN
F 3 "~" H 8250 4100 50  0001 C CNN
	1    8250 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R1
U 1 1 60B709C0
P 8650 3950
F 0 "R1" V 8445 3950 50  0000 C CNN
F 1 "R_Small_US" V 8536 3950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 8650 3950 50  0001 C CNN
F 3 "~" H 8650 3950 50  0001 C CNN
	1    8650 3950
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small_US R2
U 1 1 60B712F8
P 9400 4050
F 0 "R2" H 9332 4004 50  0000 R CNN
F 1 "R_Small_US" H 9332 4095 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 9400 4050 50  0001 C CNN
F 3 "~" H 9400 4050 50  0001 C CNN
	1    9400 4050
	-1   0    0    1   
$EndComp
Wire Wire Line
	9400 3950 9000 3950
Wire Wire Line
	9000 3950 8750 3950
Connection ~ 9000 3950
Wire Wire Line
	8550 3950 8250 3950
Wire Wire Line
	8250 3950 8250 4000
Wire Wire Line
	8250 3950 7600 3950
Wire Wire Line
	7600 3950 7600 4000
Connection ~ 8250 3950
Wire Wire Line
	7600 3950 7600 3800
Connection ~ 7600 3950
Wire Wire Line
	7600 3500 7300 3500
Wire Wire Line
	7000 3800 7000 3900
Wire Wire Line
	7600 4300 7300 4300
Wire Wire Line
	7300 4300 7300 4350
Wire Wire Line
	7300 4350 6800 4350
Wire Wire Line
	5800 4350 5800 4150
Connection ~ 7300 4300
Wire Wire Line
	7300 4300 7000 4300
Wire Wire Line
	5800 3750 5800 3450
Wire Wire Line
	5800 3450 7300 3450
Wire Wire Line
	7300 3450 7300 3500
Connection ~ 7300 3500
Wire Wire Line
	7300 3500 7000 3500
Wire Wire Line
	7000 3900 6800 3900
Wire Wire Line
	6800 3900 6800 4350
Connection ~ 7000 3900
Wire Wire Line
	7000 3900 7000 4000
Connection ~ 6800 4350
Wire Wire Line
	6800 4350 5800 4350
Wire Wire Line
	8250 4200 8250 4350
Wire Wire Line
	8250 4350 7300 4350
Connection ~ 7300 4350
Wire Wire Line
	8250 4350 9000 4350
Wire Wire Line
	9000 4350 9000 4250
Connection ~ 8250 4350
Wire Wire Line
	9000 4350 9400 4350
Wire Wire Line
	9400 4350 9400 4150
Connection ~ 9000 4350
Wire Wire Line
	5800 4350 5000 4350
Connection ~ 5800 4350
Wire Wire Line
	5000 4150 5000 4350
Wire Wire Line
	5000 3750 4700 3750
Wire Wire Line
	9400 3950 9700 3950
Connection ~ 9400 3950
Text HLabel 9700 3950 2    50   Input ~ 0
R_Vout
Text HLabel 4700 3750 0    50   Input ~ 0
Plug
Wire Wire Line
	5000 4350 4700 4350
Connection ~ 5000 4350
Text HLabel 4700 4350 0    50   Input ~ 0
GND
$Comp
L Device:Transformer_1P_1S T1
U 1 1 60B69287
P 5400 3950
F 0 "T1" H 5400 4331 50  0000 C CNN
F 1 "Transformer_1P_1S" H 5400 4240 50  0000 C CNN
F 2 "Transformer_THT:Transformer_NF_ETAL_P3356" H 5400 3950 50  0001 C CNN
F 3 "~" H 5400 3950 50  0001 C CNN
	1    5400 3950
	1    0    0    -1  
$EndComp
$EndSCHEMATC
