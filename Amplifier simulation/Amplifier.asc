Version 4
SHEET 1 880 680
WIRE 256 -224 160 -224
WIRE 256 -160 256 -224
WIRE 160 -112 160 -144
WIRE 160 -112 96 -112
WIRE 304 -112 160 -112
WIRE 304 -32 304 -112
WIRE 96 32 96 -112
WIRE 400 48 304 48
WIRE 304 96 304 48
WIRE 96 144 96 112
WIRE 240 144 96 144
WIRE 96 208 96 144
WIRE 304 224 304 192
WIRE 416 224 304 224
WIRE 304 240 304 224
WIRE 416 240 416 224
WIRE 416 336 416 304
WIRE 96 352 96 288
WIRE 304 352 304 320
FLAG 256 -160 0
FLAG 416 336 0
FLAG 304 352 0
FLAG 96 352 0
FLAG 400 48 out
SYMBOL res 288 -48 R0
SYMATTR InstName R3
SYMATTR Value 9k
SYMBOL res 288 224 R0
SYMATTR InstName R4
SYMATTR Value 500
SYMBOL npn 240 96 R0
SYMATTR InstName Q1
SYMATTR Value 2N2222
SYMBOL cap 400 240 R0
SYMATTR InstName C2
SYMATTR Value 47�
SYMBOL voltage 160 -128 R180
WINDOW 0 24 96 Left 2
WINDOW 3 24 16 Left 2
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V2
SYMATTR Value 5
SYMBOL res 80 16 R0
SYMATTR InstName R1
SYMATTR Value 100
SYMBOL res 80 192 R0
SYMATTR InstName R2
SYMATTR Value 2k
TEXT -162 376 Left 2 !.tran 5M
